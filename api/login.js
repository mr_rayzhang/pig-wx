import http from '@/utils/request'
import qs from 'qs'
const scope = 'server'

export const loginByUsername = (username, password, code, randomStr) => {
  let grant_type = 'password'
  let dataObj = qs.stringify({'username': username, 'password': password})

  return http.request({
    url: '/auth/oauth/token',
    header: {
      isToken: false,
      'TENANT-ID': '1',
      'Authorization': 'Basic cGlnOnBpZw=='
    },
    method: 'post',
    params: {randomStr, code, grant_type},
    data: dataObj
  })
}

export const refreshToken = (refresh_token) => {
  const grant_type = 'refresh_token'
  return http.request({
    url: '/auth/oauth/token',
    header: {
      'isToken': false,
      'TENANT-ID': '1',
      'Authorization': 'Basic cGlnOnBpZw=='
    },
    method: 'post',
    params: {refresh_token, grant_type, scope}
  })
}

//绑定小程序1003
export const loginByMobile = (mobile, code) => {
  const grant_type = 'mobile'
  return http.request({
    url: '/auth/mobile/token/sms',
    header: {
      isToken: false,
      'TENANT-ID': '1',
      'Authorization': 'Basic cGlnOnBpZw=='
    },
    method: 'post',
    params: {mobile: 'SMS@' + mobile, code: code, grant_type}
  })
}

//小程序自动授权登录1001
export const loginBySocial = (state, code) => {
  const grant_type = 'mobile'
  return http.request({
    url: '/auth/mobile/token/social',
    header: {
      isToken: false,
      'TENANT-ID': '1',
      'Authorization': 'Basic cGlnOnBpZw=='
    },
    method: 'post',
    params: {mobile: state + '@' + code, grant_type}
  })
}

export const getUserInfo = () => {
  return http.request({
    url: '/admin/user/info',
    method: 'get'
  })
}

export const logout = () => {
  return http.request({
    url: '/auth/token/logout',
    method: 'delete'
  })
}

// 绑定小程序1002
export const staffPhone = (phone) => {
  return http.request({
    url: '/doc/staff/phone/' + phone,
    method: 'get',
  })
}

// 绑定小程序1004
export const socialBind = (state, code) => {
  return http.request({
    url: '/admin/social/bind',
    method: 'post',
    params: {state: state, code: code}
  })
}

export const getPhone = (code) => {
  return http.request({
    url: '/admin/mobile/mini/' + code,
    method: 'get'
  })
}
