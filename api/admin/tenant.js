import http from '@/utils/request'

export function fetchPage(query) {
  return http.request({
    url: '/admin/tenant/page',
    method: 'get',
    params: query
  })
}

export function fetchList(query) {
  return http.request({
    url: '/admin/tenant/list',
    method: 'get',
    params: query
  })
}
